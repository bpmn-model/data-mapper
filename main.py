#!/usr/bin/python
#encoding: utf-8

import logging 
import glob 
import argparse # for parsing input arguments
import sys
import os

import render_code

def read_file(path):
    contents = ""
    with open(path) as file:  
        contents = file.read() 
    if not contents:
        raise Exception ("File not found at path " + path + " or empty")
    return contents

def write_file(path, content):
    with open(path, 'w') as file:  
        file.write(content) 

###########################################################
# Command line handling
###########################################################

# passable parameters
passable_parameters = {
    "output": {"abbr": "o", "name": "output", "help": "Output file", "type": str, "required": True},
    "schema": {"abbr": "s", "name": "schema", "help": "Schema file", "type": str, "required": True},
    "template": {"abbr": "t", "name": "template", "help": "Template file", "type": str, "required": True}
}

def create_arguments():
    parser = argparse.ArgumentParser(description='Running NAME dispersion model')

    # add all optional parameters
    for p in passable_parameters:
        param = passable_parameters[p]
        parser.add_argument(
            '-' + param["abbr"],
            '--' + param["name"].replace("_", "-"), 
            required=param["required"],
            type=param["type"],
            help=param["help"]
        )
    return parser.parse_args()    

def collect_arguments(cmd_args):     
    # lets collect the rest of our variables that we want to override through our CLI
    cmd_dict = vars(cmd_args)
    return cmd_dict  

###########################################################
# Main
###########################################################

def main():
    if 1:#try:

        param = collect_arguments(create_arguments())

        # load files
        template_file = read_file(param["template"])

        
        # write result
        write_file(param["output"], generated_code)
        
    #except Exception as e:
    #    print ("Executing failed due error: {}".format(str(e)))
    #    exit(1) # let system know there is an error

if __name__== "__main__":
    main()

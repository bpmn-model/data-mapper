import unittest
import copy 

# module to test
import data_mapper

class TestMappingJavascriptSimple(unittest.TestCase):
    def setUp(self):
        self.person_data = copy.deepcopy(person_data)
        self.person_data_schema = copy.deepcopy(person_data_schema)

    def test_that_schema_validates_input_successfully(self):
        """shoud be able to validate simple schema"""
        self.assertDictEqual(data_mapper.validate_data(self.person_data, self.person_data_schema), person_data)

    def test_that_schema_validates_input_raisesSchemaDataException(self):
        """should be able to raise SchemaDataException if input data is wrong"""
        del self.person_data["first_name"]
        self.assertRaises(data_mapper.SchemaDataException, data_mapper.validate_data, self.person_data, self.person_data_schema)
    
    def test_that_transform_without_transform(self):
        """should be able to fail output validation"""
        # TODO:
        pass


class TestMappingJMESPathSimple(unittest.TestCase):
        
    def setUp(self):
        self.person_data = copy.deepcopy(person_data)
        self.person_data_schema = copy.deepcopy(person_data_schema)

    def test_that_transform_data_into_full_name(self):
        """should be able to transform_data simple top level removal of first_name"""
        transform = copy.deepcopy(self.person_data_schema)
        # define the transformation for this test 
        transform["properties"]["full_name"] = transform["properties"]["first_name"]
        transform["required"] = [
            "full_name",
            "age",
            "address",
            "parents"
        ]
        transform["properties"]["full_name"]["transform"] = { # extract full_name from first_name and last_name
            "type": "jmespath",
            "code": "[first_name, last_name] | join(' ', @)"
        }
        del transform["properties"]["first_name"]
        del transform["properties"]["last_name"]
        
        # do the actual transformation
        result = data_mapper.transform_data(self.person_data, self.person_data_schema, transform)
        # test that we got correct data back against schema
        self.assertTrue(data_mapper.validate_data(result, transform).keys(), result.keys())
        # test the transformed value (actual changes)
        self.assertEqual(result["full_name"], "Jon Doe")


class TestMappingJavascriptSimple(unittest.TestCase):
        
    def setUp(self):
        self.person_data = copy.deepcopy(person_data)
        self.person_data_schema = copy.deepcopy(person_data_schema)

    def test_that_transform_data_into_full_name(self):
        """should be able to transform_data simple top level removal of first_name"""
        transform = copy.deepcopy(self.person_data_schema)
        # define the transformation for this test 
        transform["properties"]["full_name"] = transform["properties"]["first_name"]
        transform["required"] = [
            "full_name",
            "age",
            "address",
            "parents"
        ]
        transform["transform"] = {
            "type": "javascript",
            "code": "function _T(obj){ obj['full_name'] = obj['first_name'] + ' ' + obj['last_name']; obj['first_name'] = obj['last_name'] = undefined; return obj; };"
        }
        del transform["properties"]["first_name"]
        del transform["properties"]["last_name"]
        
        # do the actual transformation
        result = data_mapper.transform_data(self.person_data, self.person_data_schema, transform)
        # test that we got correct data back against schema
        self.assertTrue(data_mapper.validate_data(result, transform).keys(), result.keys())
        # test the transformed value (actual changes)
        self.assertEqual(result["full_name"], "Jon Doe")

    def test_that_transformation_into_full_name(self):
        """should be able to transform simple top level removal of first_name"""
        transform = copy.deepcopy(self.person_data_schema)
        # define the transformation for this test 
        transform["properties"]["full_name"] = transform["properties"]["first_name"]
        transform["required"] = [
            "full_name",
            "age",
            "address",
            "parents"
        ]
        transform["transform"] = {
            "type": "javascript",
            "code": "function _T(obj){ obj['full_name'] = obj['first_name'] + ' ' + obj['last_name']; obj['first_name'] = obj['last_name'] = undefined; return obj; };"
        }
        del transform["properties"]["first_name"]
        del transform["properties"]["last_name"]
        
        # do the actual transformation
        result = data_mapper.transform(self.person_data, self.person_data_schema, transform)
        # test the transformed value (actual changes)
        self.assertEqual(result["full_name"], "Jon Doe")

    ################ more advanced ################

    def test_that_transformation_into_street_data_schema(self):
        """should be able to transform into street_data_schema"""
        # do the actual transformation
        result = data_mapper.transform(self.person_data, self.person_data_schema, street_data_transform_javascript)
        # test the transformed value (actual changes)
        self.assertDictEqual(result, street_data)

    def test_that_transformation_into_street_data_schema_changingtransformtocapitalpersons(self):
        """should be able to transform into street_data_schema running sub execution on transformed data"""
        transform = copy.deepcopy(street_data_transform_javascript)
        # append transformation making upper case 
        transform["properties"]["persons"]["transform"] = {
            "type": "javascript",
            "code": """
            function _T(src){
                var trg = [ ];
                for (s in src){ trg.push( src[s].toUpperCase() ); }
                return trg; 
            };
            """
        }                     
        # do the actual transformation
        result = data_mapper.transform(self.person_data, self.person_data_schema, transform)
        # test the transformed value (actual changes)
        self.assertListEqual(result["persons"], ["MISSES DOE", "MR JON", "JON DOE"])        

person_data = {
    "first_name": "Jon",
    "last_name": "Doe",
    "age": 22,
    "address": {
        "street address": "Kennedy Street",
        "zip": "42243"
    },
    "parents": ["Misses Doe", "Mr Jon"]
}

street_data = {
    "street address": "Kennedy Street",
    "zip": "42243",
    "persons": ["Misses Doe", "Mr Jon", "Jon Doe"]
}

person_data_schema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "properties": {
        "first_name": {
            "type": "string"
        },
        "last_name": {
            "type": "string"
        },
        "age": {
            "type": "integer"
        },
        "address": {
            "type": "object",
            "properties": {
                "street address": {
                    "type": "string"
                },
                "zip": {
                    "type": "string"
                }
            },
            "required": [
                "street address",
                "zip"
            ]
        },
        "parents": {
            "type": "array",
            "items": [
                {
                    "type": "string"
                },
                {
                    "type": "string"
                }
            ]
        }
    },
    "required": [
        "first_name",
        "last_name",
        "age",
        "address",
        "parents"
    ]
}

street_data_transform_javascript = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "transform":{
        "type": "javascript",
        "code": """
        function _T(src){
            var trg = { };
            trg["street address"] = src["address"]["street address"];
            trg["zip"] = src["address"]["zip"];
            trg["persons"] = src["parents"]
            trg["persons"].push(src['first_name'] + ' ' + src['last_name'])
            return trg; 
        };
        """
    },
    "properties": {
        "street address": {
            "type": "string"
        },
        "zip": {
            "type": "string"
        },
        "persons": {
            "type": "array",
            "items": [
                {
                    "type": "string"
                },
                {
                    "type": "string"
                },
                {
                    "type": "string"
                }
            ]
        }
    },
    "required": [
        "street address",
        "zip",
        "persons"
    ]
}

# "persons": ["Misses Doe", "Mr Jon", "Jon Doe"]
street_data_transform_jmespath = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "properties": {
        "street address": {
            "type": "string",
            "transform":{
                "type": "jmespath",
                "code": 'address."street address"'
            }
        },
        "zip": {
            "type": "string",
            "transform":{
                "type": "jmespath",
                "code": "address.zip"
            }
        },
        "persons": {
            "type": "array",
            "transform": {
                "type": "jmespath",
                "code": "[join(' ', [first_name, last_name])]"
            },
            "items": [
                {
                    "type": "string"
                },
                {
                    "type": "string"
                },
                {
                    "type": "string"
                }
            ]
        }
    },
    "required": [
        "street address",
        "zip",
        "persons"
    ]
}

if __name__ == '__main__':
    unittest.main()


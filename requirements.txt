attrs==19.3.0
importlib-metadata==1.6.0
jsonschema==3.2.0
jmespath==0.10.0
pyrsistent==0.16.0
quickjs==1.12.0
six==1.14.0
zipp==3.1.0

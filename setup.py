#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='data-mapper',
      version='0.1.1',
      description='Mapping JSON data to target JSON schema given input and output JSON-Schema',
      author='ProcessModel',
      url='http://github.com/kajjjak/',
      classifiers=['Programming Language :: Python :: 3 :: Only'],
      py_modules=['data_mapper'],
      install_requires=[
          'attrs==19.3.0',
          'importlib-metadata==1.6.0',
          'jsonschema==3.2.0',
          'jmespath==0.10.0',
          'pyrsistent==0.16.0',
          'quickjs==1.12.0',
          'six==1.14.0',
          'zipp==3.1.0'
          
      ],
      extras_require={
          'dev': [
              'ipdb==0.11'
          ]
      },
      entry_points='''
          [console_scripts]
          data_mapper=main:main
      ''',
      packages=['data_mapper'],
      include_package_data=True
)

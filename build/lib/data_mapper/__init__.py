# json
import json
import jsonschema
import copy

# https://github.com/PetterS/quickjs
from quickjs import Function # pip install quickjs
import jmespath # pip install jsonschema
TRANSFORM_TYPES = ["javascript", "jmespath"]

# Exceptions
class IntegrityException(Exception): pass
class SchemaDataException(Exception): pass
class TransformException(Exception): pass

def validate_data(data_dict, schema_definition):
    """Loads data source validating it against the schema

    Arguments:
        data {object} -- the data to validate
        schema {object} -- the json-schema to validate data against

    Raises:
        SchemaDataException: if data does not match schema

    Returns:
        {object} -- unchanged validated data

    """
    # validate input data
    try:     
        jsonschema.validate(instance=data_dict, schema=schema_definition)
    except jsonschema.ValidationError as e:
        raise SchemaDataException("Data does not match schema. Got error: " + str(e))
    return data_dict

def _transform_recursion(data, schema_transform, full_data):
    """Recursive function that descends schema_transform adding values to data_out

    Arguments:
        data {object} -- input data
        schema_transform {object} -- the sub json-schema to use for transformation

    Raises:
        SchemaDataException: If data does not correspont to schema

    Returns:
        {object} -- transformed data according to schema_transform        
    """
    # for this level of schema_transform, lets check if there is any transform
    transf_data = data
    if "transform" in schema_transform:
        transform = schema_transform["transform"]
        if not "type" in transform and transform["type"] in TRANSFORM_TYPES:
            raise IntegrityException("Transformer needs type defined and type values being one of " + str(TRANSFORM_TYPES))
        if not "code" in transform:
            raise IntegrityException("Transformer needs code to be defined")        
        if transform["type"] == "javascript":
            try:
                function = Function("_T", transform["code"])
                # prepare schema transform subset
                schema_transform_subset = copy.deepcopy(schema_transform)
                del schema_transform_subset["transform"]
                # passing current data scope, full data (unchanged) and the current schema transform (without transform section)
                transf_data = function(data, full_data, schema_transform_subset)
            except Exception as e:
                raise TransformException("Could not run javascript. " + str(transform["code"]) + " Got this error " + str(e))
        elif transform["type"] == "jmespath": # https://jmespath.org/specification.html
            try:
                transf_data = jmespath.search(transform["code"], full_data)
            except Exception as e:
                raise TransformException("Could not run JMESPath. " + str(transform["code"]) + " Got this error " + str(e))
                
    # lets check for sub objects for transition functions
    if "properties" in schema_transform:
        for schema_prop in schema_transform["properties"]:
            # extract default value, is default value of none to begin with
            value = None 
            if schema_prop in transf_data:
                value = transf_data[schema_prop]
            elif "default" in schema_transform["properties"][schema_prop]:
                value = schema_transform["properties"][schema_prop]["default"]
            transf_data[schema_prop] = _transform_recursion (value, schema_transform["properties"][schema_prop], full_data)
    # return transformed data            
    return transf_data

# parse and run transform
def transform_data (data, schema_input, schema_transform):
    """Transforms data from schema_input to schema_transform

    Arguments:
        data {object} -- the data to validate
        schema_input {object} -- the json-schema to validate data against
        schema_transform {object} -- a json-schema base transform file

    Raises:
        SchemaDataException: If data does not correspont to schema

    Returns:
        object -- transformed data
    """
    # data validation
    data = validate_data(data, schema_input)
    
    # enumerate the schema and execute transform
    return _transform_recursion (data, schema_transform, copy.deepcopy(data))

def transform (data, schema_input, schema_transform):
    """Transforms data from schema_input to schema_transform

    Arguments:
        data {object} -- the data to validate
        schema_input {object} -- the json-schema to validate data against
        schema_transform {object} -- a json-schema base transform file

    Raises:
        SchemaDataException: If data does not correspont to schema

    Returns:
        object -- according to defined schema_transform json-schema definition
    """
    # transform data
    transformed = transform_data(data, schema_input, schema_transform)
    return validate_data(transformed, schema_transform)
    